### 玖玖云ERP系统、支持多平台订单同步，仓库发货，波次拣货，售后服务，电商ERP一站式解决方案


**项目介绍** 

玖玖云ERP系统基于java+springboot+element-plus+uniapp打造的面向开发的电商管理ERP系统，方便二次开发或直接使用，可发布到多端，包括微信小程序、微信公众号、QQ小程序、支付宝小程序、字节跳动小程序、百度小程序、android端、ios端。


官网地址：http://www.jjjshop.net/      


 **如果对您有帮助，您可以点右上角 "Star" 支持一下，这样我们才有继续免费下去的动力，谢谢！
QQ交流群 (入群前，请在网页右上角点 "Star" )** 

交流QQ群：970137749



 **后台截图** 

| ![输入图片说明](https://www.jjjshop.net/gitee/erp-java/1.png "1.png") | ![输入图片说明](https://www.jjjshop.net/gitee/erp-java/2.png "2.png") |
|---|---|
| ![输入图片说明](https://www.jjjshop.net/gitee/erp-java/3.png "3.png") | ![输入图片说明](https://www.jjjshop.net/gitee/erp-java/4.png "4.png") |
| ![输入图片说明](https://www.jjjshop.net/gitee/erp-java/5.png "5.png") |  ![输入图片说明](https://www.jjjshop.net/gitee/erp-java/6.png "6.png")  |




 **软件架构**

后端：springboot管理端页面：element-plus 小程序端：uniapp。

部署环境建议：Linux + Nginx + springboot + MySQL5.7/8.0，上手建议直接用宝塔集成环境。

 **安装教程、开发文档、操作手册请进入官网查询** 

[官网链接](http://www.jjjshop.net)

[安装文档](https://doc.jjjshop.net/ErpJava)

 **bug反馈**

如果你发现了bug，请发送邮件到 bug@jiujiujia.net，我们将及时修复并更新。 

 **特别鸣谢** 
- element-plus:[https://element-plus.gitee.io/zh-CN/](https://element-plus.gitee.io/zh-CN/)
- vue:[https://cn.vuejs.org/](https://cn.vuejs.org/)